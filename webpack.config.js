const path = require('path')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const MiniCssExtractPlugin = require('mini-css-extract-plugin')

module.exports = {
    entry:'./src/app/index.tsx',
    output:{
        path:path.resolve(__dirname,'build'),
        filename:'bundle.js'
    },
    module:{
        rules:[
            {
                test:/\.tsx?/,
                loader:'awesome-typescript-loader'
            },
            {
                test:/\.js$/,
                loader:'source-map-loader'
            },
            {
                test:/\.scss$/,
                use:[MiniCssExtractPlugin.loader,'css-loader','sass-loader']
            }
        ]
    },
    plugins:[
        new HtmlWebpackPlugin({
            template:'./index.html'
        }),
        new MiniCssExtractPlugin({
            filename:'style.css'
        })
    ],
    devtool:'source-map',
    resolve:{
        extensions:['.tsx','.ts','.js']
    }
}