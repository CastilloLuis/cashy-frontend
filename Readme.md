# React Project

### Config and libraries used

- webpack and webpack-dev-server
- entry point : src/app/index.tsx
- eslint +prettier + typescript
- styled-components
- storybook

#### Reference urls

[styled-components with typescript](https://blog.agney.dev/styled-components-&-typescript/)

[medium post as guide for webpack config ](https://levelup.gitconnected.com/react-typescript-with-webpack-2fceebb8faf)

[styled-components docs ](https://styled-components.com/docs/basics)

[Prettier config file rules docs](https://prettier.io/docs/en/configuration.html)

[referencia rutas con react sin react-router-dom ](https://github.com/javierh2k/workshop-react-avanzado-taller-1/blob/master/src/routes.js)

[referencia rutas con react-router-dom ](https://reacttraining.com/react-router/web/guides/quick-start)
