import * as React from 'react'

import * as ReactDOM from 'react-dom'
import App from './app'
import './app.scss'
const ROOT = document.querySelector('.container')

ReactDOM.render(<App name='CARLOS'/>,ROOT)