import * as React from 'react'



export default function App({name}:IProps) {
    return(
            <h1>Hello {name}</h1>
    )
}

interface IProps{
    name:string
}