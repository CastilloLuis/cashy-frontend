
import React from "react";
import { withInfo } from "@storybook/addon-info";
import { addDecorator, configure } from "@storybook/react";

addDecorator(
  withInfo({
    inline: true,
    header: false
  })
);

const req = require.context("../src/stories", true, /\.stories\.ts(x)?$/);

function loadStories() {
  req
    .keys()
    .sort()
    .forEach(filename => req(filename));
}

configure(loadStories, module);